import { Component, OnInit, ViewChild } from '@angular/core';
import { confirm } from 'devextreme/ui/dialog';
import { v4 as uuid } from 'uuid';
import notify from 'devextreme/ui/notify';
import { DxFormComponent } from 'devextreme-angular';

enum PlayerTypeEnum {
  machine = 'machine',
  person = 'person'
}

enum DominoPositionEnum {
  right = 'right',
  left = 'left'
}

class Player {
  id: string;
  type: PlayerTypeEnum;
  name: string;
  color: string;

  constructor(
    name: string,
    color: string,
    type: PlayerTypeEnum
  ) {
    this.id = uuid();
    this.name = name;
    this.color = color;
    this.type = type;
  }
}

class TurnBlock {
  player: Player;

  constructor(player?: Player) {
    this.player = player ? player : null;
  }
}

class Domino {
  id: string;
  leftSideNumber: number;
  rightSideNumber: number;
  position: DominoPositionEnum;

  constructor(
    leftSideNumber: number,
    rightSideNumber: number,
  ) {
    this.id = uuid();
    this.leftSideNumber = leftSideNumber;
    this.rightSideNumber = rightSideNumber;
    this.position = DominoPositionEnum.right;
  }
}

class TurnForm {
  chosenDomino: Domino;
  flip: boolean;
  currentPlayer: Player;

  constructor(
    currentPlayer: Player,
  ) {
    this.currentPlayer = currentPlayer;
  }
}

enum ModeEnum {
  configurating,
  playing
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  FIELD_REQUIRED_MESSAGE = 'El campo es requerido';
  BUTTON_WIDTH = '3em';

  playerTypeEnum = PlayerTypeEnum;
  modeEnum = ModeEnum;
  currentMode: ModeEnum = ModeEnum.configurating;
  startGameButtonOptions: any = {};
  resetGameFormButtonOptions: any = {};
  turnForm: TurnForm = new TurnForm(null);
  resetGameButtonOptions: any = {};
  returnToConfigButtonOptions: any = {};
  // tslint:disable-next-line: max-line-length
  defaultPlayers: Player[] = [new Player('Maquina', '#e63946', PlayerTypeEnum.machine), new Player('Jugador', '#023e8a', PlayerTypeEnum.person)];
  players: Player[] = [...this.defaultPlayers];
  initialPlayer: Player = null;
  dominoPositionList = [{ value: DominoPositionEnum.left, name: 'Izquierda' }, { value: DominoPositionEnum.right, name: 'Derecha' }];
  // Todos los dominos
  allDominos: Domino[] = [];
  // Dominos disponibles
  availableDominos: Domino[] = [];
  // Dominos jugados
  playedDominos: Domino[] = [];
  // Mano del jugador
  playerDominoHand: Domino[] = [];
  // Mano de la maquina
  machineDominoHand: Domino[] = [];

  @ViewChild('chosenDominoform') chosenDominoform: DxFormComponent;

  constructor() {
  }

  ngOnInit(): void {
    // Setear opciones botones del toolbar
    this.setToolBarButtonOptions();
    this.setAllDominos();
  }

  setToolBarButtonOptions = () => {
    // Opciones botón comenzar juego
    this.startGameButtonOptions = {
      icon: 'todo',
      hint: 'Comenzar juego',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => {
        if (this.players.length < 2) {
          return notify('🤨 Por favor añadir al menos 2 jugadores', 'info', 2000);
        }
        this.onStartGame();
      }
    };
    // Opciones botón resetear formulario del juego
    this.resetGameFormButtonOptions = {
      icon: 'refresh',
      hint: 'Resetear formulario',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => this.onResetForm()
    };
    // Opciones botón restear juego
    this.resetGameButtonOptions = {
      icon: 'refresh',
      hint: 'Resetear juego',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => this.onConfirmResetCurrentGame()
    };
    // Opciones botón regresar a configuración del juego
    this.returnToConfigButtonOptions = {
      icon: 'revert',
      hint: 'Regresar a configuración',
      type: 'normal',
      stylingMode: 'outlined',
      width: this.BUTTON_WIDTH,
      onClick: () => this.onConfirmReturnToConfig()
    };
  }

  setAllDominos = () => {
    this.allDominos.push(new Domino(0, 0));
    this.allDominos.push(new Domino(0, 1));
    this.allDominos.push(new Domino(0, 2));
    this.allDominos.push(new Domino(0, 3));
    this.allDominos.push(new Domino(0, 4));
    this.allDominos.push(new Domino(0, 5));
    this.allDominos.push(new Domino(0, 6));
    this.allDominos.push(new Domino(1, 1));
    this.allDominos.push(new Domino(1, 2));
    this.allDominos.push(new Domino(1, 3));
    this.allDominos.push(new Domino(1, 4));
    this.allDominos.push(new Domino(1, 5));
    this.allDominos.push(new Domino(1, 6));
    this.allDominos.push(new Domino(2, 2));
    this.allDominos.push(new Domino(2, 3));
    this.allDominos.push(new Domino(2, 4));
    this.allDominos.push(new Domino(2, 5));
    this.allDominos.push(new Domino(2, 6));
    this.allDominos.push(new Domino(3, 3));
    this.allDominos.push(new Domino(3, 4));
    this.allDominos.push(new Domino(3, 5));
    this.allDominos.push(new Domino(3, 6));
    this.allDominos.push(new Domino(4, 4));
    this.allDominos.push(new Domino(4, 5));
    this.allDominos.push(new Domino(4, 6));
    this.allDominos.push(new Domino(5, 5));
    this.allDominos.push(new Domino(5, 6));
    this.allDominos.push(new Domino(6, 6));
  }


  /**
   * Encargado de resetear el formulario
   */
  onResetForm = async () => {
    if (await this.awaitConfirm('Esta seguro de querer resetear la cantidad de jugadores?')) {
      this.players = [...this.defaultPlayers]
    }
  }

  /**
   * Encargado de crear el juego
   */
  onStartGame = () => {
    this.setInitialPlayerTurn();
    this.setGame();
    this.initialPlayer = this.turnForm.currentPlayer;
    this.currentMode = ModeEnum.playing;
    this.turnForm = new TurnForm(this.initialPlayer);
  }

  setGame = () => {
    // Setear dominos disponibles
    this.availableDominos = [...this.allDominos];
    // Quitar la mula de seis
    this.playedDominos = this.availableDominos.filter(dom => dom.leftSideNumber === 6 && dom.rightSideNumber === 6);
    this.availableDominos = this.removeDominosFromList(this.playedDominos, this.availableDominos);
    // Setear mano del jugador
    this.playerDominoHand = this.getRandomSetOfDominosFromList(this.availableDominos);
    this.availableDominos = this.removeDominosFromList(this.playerDominoHand, this.availableDominos);
    // Setear mano de la maquina
    this.machineDominoHand = this.getRandomSetOfDominosFromList(this.availableDominos);
    this.availableDominos = this.removeDominosFromList(this.machineDominoHand, this.availableDominos);
  }

  onChooseDominoFromAvailable = (chosenDomino: Domino, playerType: PlayerTypeEnum) => {
    this.availableDominos = this.removeDominosFromList([chosenDomino], this.availableDominos);
    if (playerType === PlayerTypeEnum.person) {
      this.playerDominoHand.push(chosenDomino);
    } else {
      this.machineDominoHand.push(chosenDomino);
    }
    this.definWinner();
  }

  removeDominosFromList = (dominosToRemove: Domino[], dominoList: Domino[]) => {
    const updatedList = dominoList.filter(dom => {
      // tslint:disable-next-line: max-line-length
      const dominoFoundInDominosToRemove = dominosToRemove.find(d => d.id === dom.id);
      if (!dominoFoundInDominosToRemove) {
        return dom;
      }
    });
    return updatedList;
  }

  getRandomSetOfDominosFromList = (dominoList: Domino[]): Domino[] => {
    const randomSet: Domino[] = [];
    let availableDominoList = [...dominoList];
    // Crear listado de dominos aleatoriamente
    while (randomSet.length < 8) {
      // Obtener un domino aleatorio
      const randomIndex = Math.floor(Math.random() * availableDominoList.length);
      const randomDomino = availableDominoList[randomIndex];
      // Si no se ha añadido ningun domino antes
      if (randomSet.length === 0) {
        // añadir
        randomSet.push(randomDomino);
        availableDominoList = this.removeDominosFromList(randomSet, availableDominoList);
      }
      // En caso de no haber añadido a lista aleatoria
      else {
        // Verificar que no se haya añadido a lista aleatoria
        // tslint:disable-next-line: max-line-length
        if (!randomSet.find(domino => domino.leftSideNumber !== randomDomino.leftSideNumber && domino.rightSideNumber !== domino.rightSideNumber)) {
          randomSet.push(randomDomino);
          availableDominoList = this.removeDominosFromList(randomSet, availableDominoList);
        }
      }
    }
    return randomSet;
  }

  onConfirmResetCurrentGame = async () => {
    if (await this.awaitConfirm('Esta seguro de querer reiniciar la partida?')) {
      this.onStartGame();
    }
  }

  onConfirmReturnToConfig = async () => {
    if (await this.awaitConfirm('Si se regresa se perdera la partida, esta seguro de querer continuar?')) {
      this.onReturnToConfig();
    }
  }

  /**
   * Encargado de regresar a modo config
   */
  onReturnToConfig = () => {
    this.currentMode = ModeEnum.configurating;
  }

  /**
   * Setea turno del jugador inicial
   */
  setInitialPlayerTurn = () => {
    this.turnForm.currentPlayer = this.getRandomPlayer();
  }

  /**
   * Dialogo confirmar generico
   */
  async awaitConfirm(message: string): Promise<boolean> {
    const c = await confirm(message, 'Confirmar!');
    return c;
  }

  /**
   * Obtiene un color aleatorio
   */
  getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  /**
   * Obtiene un jugador aleatorio
   */
  getRandomPlayer = (): Player => {
    if (!this.players || this.players.length === 0) {
      console.error('No se obtuvo listado de jugadores');
      return null;
    }
    return this.players[Math.floor(Math.random() * this.players.length)];
  }

  onAddPlayerDomino = () => {
    if (!this.chosenDominoform.instance.validate()) {
      return;
    }
    if (!this.validateDominoAddition(this.turnForm.chosenDomino)) {
      return notify('🤨 Domino incorrecto', 'error', 2000);
    }
    this.onAddDomino(this.turnForm.chosenDomino);
    this.playerDominoHand = this.removeDominosFromList([this.turnForm.chosenDomino], this.playerDominoHand);
    this.turnForm = new TurnForm(null);
    this.handleMachineTurn();
    this.definWinner();
  }

  definWinner = async () => {
    let draw = false;
    let winner: Player = null;
    if (this.playerDominoHand.length === 0) {
      winner = this.players.find(p => p.type === PlayerTypeEnum.person);
    }
    if (this.machineDominoHand.length === 0) {
      winner = this.players.find(p => p.type === PlayerTypeEnum.machine);
    }
    if (this.availableDominos.length === 0) {
      // En caso de ya no haber tarjetas disponibles determinar si jugador tiene tarjeta valida
      const foundValidDominoInPlayerHand = this.getUsableDominoFromList(this.playerDominoHand);
      if (!foundValidDominoInPlayerHand) {
        // En caso de no haber mas tarjetas validas para jugador
        if (this.playerDominoHand.length === this.machineDominoHand.length) {
          draw = true;
        }
        if (this.playerDominoHand.length < this.machineDominoHand.length) {
          winner = this.players.find(p => p.type === PlayerTypeEnum.person);
        } else {
          winner = this.players.find(p => p.type === PlayerTypeEnum.machine);
        }
      }
    }
    if (draw) {
      if (await this.awaitConfirm(`Empate!!! Desean repetir jugar nuevamente?`)) {
        return this.onStartGame();
      }
    }
    if (winner) {
      if (await this.awaitConfirm(`🙂 GANASTE ${winner.name}!!! Desean jugar nuevamente?`)) {
        return this.onStartGame();
      }
    }
  }

  /**
   * Encargado de realizar el tiro
   */
  onAddDomino = (domino: Domino) => {
    // Validar que se pueda añadir el domino
    const dominoPosition: DominoPositionEnum = domino.position;
    // En caso de ser un domino valido
    if (dominoPosition === DominoPositionEnum.left) {
      this.playedDominos.unshift(domino);
    } else {
      this.playedDominos.push(domino);
    }
  }

  validateDominoAddition = (domino: Domino): boolean => {
    let isDominoValid = true;
    const dominoPosition: DominoPositionEnum = domino.position;
    if (dominoPosition === DominoPositionEnum.left) {
      const nextDomino = this.playedDominos[0];
      if (this.turnForm.chosenDomino.rightSideNumber !== nextDomino.leftSideNumber) {
        isDominoValid = false;
      }
    } else {
      const previousDomino = this.playedDominos[this.playedDominos.length - 1];
      if (this.turnForm.chosenDomino.leftSideNumber !== previousDomino.rightSideNumber) {
        isDominoValid = false;
      }
    }
    return isDominoValid;
  }


  /**
   * Encargado de determinar el perdedor o ganador de la partida
   */
  onDetermineWinnerOrLooser = async (availableTurnBlocks: TurnBlock[]) => {
    console.log('determinar jugador');
  }

  /**
   * Setea el siguiente turno
   */
  setNextTurn = () => {
    const currentPlayerIndex = this.players.findIndex((player: Player) => player.id === this.turnForm.currentPlayer.id);
    if (currentPlayerIndex !== this.players.length - 1) {
      return this.turnForm.currentPlayer = { ...this.players[currentPlayerIndex + 1] };
    }
    this.turnForm.currentPlayer = { ...this.players[0] };
  }

  /**
   * Encargado de manejar turno de maquina
   */
  handleMachineTurn = () => {
    // Verificar de dominos del set de la maquina cual puede funcionar
    const foundValidDominoInMachineHand = this.getUsableDominoFromList(this.machineDominoHand);
    if (foundValidDominoInMachineHand) {
      this.onAddDomino(foundValidDominoInMachineHand);
      this.machineDominoHand = this.removeDominosFromList([foundValidDominoInMachineHand], this.machineDominoHand);
    } else {
      // En caso de no haber encontrado un domino, buscar uno en los disponibles
      const foundDominoInAvailableDominos = this.getUsableDominoFromList(this.availableDominos);
      if (foundDominoInAvailableDominos) {
        this.onAddDomino(foundDominoInAvailableDominos);
        this.availableDominos = this.removeDominosFromList([foundDominoInAvailableDominos], this.availableDominos);
      } else {
        return notify('🤨 La maquina no encontro un domino, tomar turno', 'info', 2000);
      }
    }
  }

  getUsableDominoFromList = (dominoList: Domino[]): Domino => {
    let usableDomino = null;
    const validNumberLeft = this.playedDominos[0].leftSideNumber;
    const validNumberRight = this.playedDominos[this.playedDominos.length - 1].rightSideNumber;
    // tslint:disable-next-line: max-line-length
    let usableDominoForLeft = dominoList.find(domino => domino.leftSideNumber === validNumberLeft || domino.rightSideNumber === validNumberLeft);
    // tslint:disable-next-line: max-line-length
    let usableDominoForRight = dominoList.find(domino => domino.leftSideNumber === validNumberRight || domino.rightSideNumber === validNumberRight);

    if (usableDominoForLeft || usableDominoForRight) {
      // En caso de haber encontrado un domino que sirva para la mano actual
      if (usableDominoForLeft) {
        if (usableDominoForLeft.rightSideNumber === validNumberLeft) {
          usableDomino = { ...usableDominoForLeft };
        } else {
          usableDominoForLeft = this.flipDomino(usableDominoForLeft);
          usableDomino = { ...usableDominoForLeft };
        }
      } else {
        if (usableDominoForRight.leftSideNumber === validNumberRight) {
          usableDomino = { ...usableDominoForRight };
        } else {
          usableDominoForRight = this.flipDomino(usableDominoForRight);
          usableDomino = { ...usableDominoForRight };
        }
      }
    }

    return usableDomino;
  }

  /**
   * Obtiene un total a partir de un listado de numeros
   */
  getTotalFromNumberArray = (numberArray: number[]): number => {
    if (!numberArray || numberArray.length === 0) {
      return 0;
    }
    return numberArray.reduce((total, num) => total + num);
  }

  onDominoChosenFromPlayerHand = (chosenDomino: Domino) => {
    this.turnForm.chosenDomino = chosenDomino;
  }

  onFlipChosenDomino = (e: { value: boolean }) => {
    this.turnForm.chosenDomino = this.flipDomino(this.turnForm.chosenDomino);
  }

  flipDomino = (domino: Domino): Domino => {
    const flippedDomino = { ...domino };
    const leftSideNumber = domino.leftSideNumber;
    const rightSideNumber = domino.rightSideNumber;
    flippedDomino.leftSideNumber = rightSideNumber;
    flippedDomino.rightSideNumber = leftSideNumber;
    return flippedDomino;
  }

  onDefineSelectedDominoPosition = (e: { value: DominoPositionEnum }) => {
    this.turnForm.chosenDomino.position = e.value;
  }
}
